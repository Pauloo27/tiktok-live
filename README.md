# TikTok Live Downloader

## How to install

First, clone the repository:

> git clone https://github.com/Pauloo27/tiktok-live.git

## How to use

In the repository folder, run the command:

> npm install

This will download the dependencies. Now, you can download tiktok lives, run:

> node index.js \<url\>

Here the `<url>` is the live share URL.
